package me.danybv.maxiroleplay.helpers;

import org.bukkit.ChatColor;

/**
 * Created by Dany on 13.02.2017.
 */
public class LangHelper {

    public static String colorify(String s)
    {
        return ChatColor.translateAlternateColorCodes('&', s);
    }

    public static String getLang(String key)
    {
        return colorify(ConfigHelper.getLangConfig().getString(key));
    }
}
