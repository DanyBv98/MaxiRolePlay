package me.danybv.maxiroleplay.helpers;

import me.danybv.maxiroleplay.MaxiRolePlay;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Created by Dany on 13.02.2017.
 */
public class ConfigHelper {

    private static File langFile;
    private static YamlConfiguration langConfig;
    private static File dataFolder;

    public static void initialize(MaxiRolePlay plugin)
    {
        dataFolder = plugin.getDataFolder();
        langFile = new File(dataFolder.getPath() + File.separatorChar + "lang.yml");
        langConfig = YamlConfiguration.loadConfiguration(langFile);
        if(!langFile.exists())
        {
            try {
                InputStreamReader langConfigStream = new InputStreamReader(plugin.getResource("lang.yml"), "UTF-8");
                YamlConfiguration _langConfig = YamlConfiguration.loadConfiguration(langConfigStream);
                langConfig.setDefaults(_langConfig);
                langConfig.options().copyDefaults(true);
                langConfig.save(langFile);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public static YamlConfiguration getLangConfig() {
        return langConfig;
    }

    public File getDataFolder()
    {
        return dataFolder;
    }

}
