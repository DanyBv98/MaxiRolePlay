package me.danybv.maxiroleplay.helpers;


import me.danybv.maxiroleplay.commands.MRPCommand;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Dany on 13.02.2017.
 */
public class CommandHandler {

    HashMap<String, MRPCommand> commands = new HashMap<String, MRPCommand>();

    public CommandHandler()
    {
        Map<String, Map<String, Object>> pluginCommands = PluginHelper.getPlugin().getDescription().getCommands();
        for(String command : pluginCommands.keySet()) {
            try {
                Class<?> cmdClass = Class.forName("me.danybv.maxiroleplay.commands.Command" + command);
                Constructor<?> cmdCons = cmdClass.getConstructor();
                commands.put(command, (MRPCommand) cmdCons.newInstance());
            } catch (ClassNotFoundException | NoSuchMethodException | InstantiationException | IllegalAccessException | InvocationTargetException e) {
                e.printStackTrace();
            }
        }
    }

    public boolean handleCommand(CommandSender sender, Command command, String commandLabel, String[] args)
    {
        String commandName = command.getName();
        boolean result = commands.containsKey(commandName);
        if(result)
            commands.get(commandName).run(sender, args);
        return result;
    }
}
