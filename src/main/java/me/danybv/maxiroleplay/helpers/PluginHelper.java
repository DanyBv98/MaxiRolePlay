package me.danybv.maxiroleplay.helpers;

import me.danybv.maxiroleplay.MaxiRolePlay;
import org.bukkit.ChatColor;

/**
 * Created by Dany on 13.02.2017.
 */
public class PluginHelper {
    private static MaxiRolePlay plugin;

    public static void initialize(MaxiRolePlay plugin)
    {
        PluginHelper.plugin = plugin;
    }

    public static MaxiRolePlay getPlugin()
    {
        return plugin;
    }
}
