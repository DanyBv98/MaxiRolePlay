package me.danybv.maxiroleplay.commands;

import me.danybv.maxiroleplay.helpers.LangHelper;
import me.danybv.maxiroleplay.helpers.PluginHelper;
import org.bukkit.command.CommandSender;
import org.bukkit.plugin.PluginDescriptionFile;

import java.util.List;

/**
 * Created by Dany on 13.02.2017.
 */
public class Commandmaxiroleplay extends MRPCommand {

    @Override
    public void run(CommandSender sender, String[] args) {
        PluginDescriptionFile desc = PluginHelper.getPlugin().getDescription();
        List<String> authors = desc.getAuthors();
        String authorsString = "";
        for(int i = 0; i < authors.size() - 1; i++)
            authorsString += authors.get(i);
        authorsString += authors.get(authors.size() - 1);
        sender.sendMessage(LangHelper.colorify("&a" + desc.getName() + " was created by " + authorsString + ". Current version: v" + desc.getVersion() + "."));
    }
}
