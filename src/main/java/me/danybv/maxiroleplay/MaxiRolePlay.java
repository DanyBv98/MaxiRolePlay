package me.danybv.maxiroleplay;

import me.danybv.maxiroleplay.helpers.CommandHandler;
import me.danybv.maxiroleplay.helpers.ConfigHelper;
import me.danybv.maxiroleplay.helpers.PluginHelper;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.plugin.java.JavaPlugin;

/**
 * Created by Dany on 13.02.2017.
 */
public class MaxiRolePlay extends JavaPlugin {

    CommandHandler commandHandler;

    @Override
    public void onEnable() {
        PluginHelper.initialize(this);
        ConfigHelper.initialize(this);
        commandHandler = new CommandHandler();
    }

    @Override
    public void onDisable() { }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String commandLabel, String[] args)
    {
        return commandHandler.handleCommand(sender, command, commandLabel, args);
    }
}
